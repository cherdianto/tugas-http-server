const http = require('http');
const fs = require('fs');

function onRequest(req, res) {
    let path ="./pages/";

    console.log(req.url, req.method);

    if(req.url === '/'){
        path += "index.html";
        req.statusCode = 200;
    } else if (req.url === '/aboutus'){
        path += 'aboutus.html';
        req.statusCode = 200;
    } else if (req.url === '/blog') {
        path += 'blog.html';
        req.statusCode = 200;
    } else {
        path += '404.html';
        req.statusCode = 404;
    }

    res.setHeader('Content-Type','text/html');

    fs.readFile(path, null, (err,data) => {
        if (err) {
            res.writeHead(404);
            res.write('file not found');
        } else {
            res.write(data);
        }
        res.end();
    })
}

http.createServer(onRequest).listen(8000);